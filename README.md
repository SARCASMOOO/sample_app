# Ruby on rails sample application

This is a rails website I'm making from following along with the ruby tutorial

## Getting started

To get started with this app clone the repository then install the gems needed

---
$ bundle install --without production
---

Next migrate database

---
$ rails db:migrate
---

Finally, run the test suite to verify that everything is working correctly

---
$ rails test
---

If the test suite passes, you'll be ready to run the app on a local server

---
$ rails server
---
