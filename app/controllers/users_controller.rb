class UsersController < ApplicationController
  def show
    tempUser = User.find_by_id(params[:id])
    if tempUser.nil?
      redirect_to root_url
    else
      @user = tempUser
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = "Welcome to the sample app!"
      redirect_to @user
    else
      render 'new'
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
end
